<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Index extends CI_Controller {
  public function __construct()
 {
     parent::__construct();
 }
  var $data = array();

    public function index() {

    $data['class']="index";
    $this->load->view ( 'layout/header',$data );
    $this->load->view ( 'index.php' );
    $this->load->view ( 'layout/footer',$data );
    }
}
?>
